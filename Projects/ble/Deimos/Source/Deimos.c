/**************************************************************************************************
  Filename:       Deimos.c
  Revised:        $Date: 2014-09-01 20:35:08 +0800 (Mon, 1 Sep 2014) $
  Revision:       $Revision: 35100 $

  Description:    This file contains the Sensor Tag sample application
                  for use with the TI Bluetooth Low Energy Protocol Stack.

  Copyright 2012-2013  Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS?WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <string.h>
#include "bcomdef.h"
#include "OSAL.h"
#include "osal_snv.h"
#include "OSAL_PwrMgr.h"

#include "OnBoard.h"
#include "hal_adc.h"
#include "hal_led.h"
#include "hal_keys.h"
#include "hal_i2c.h"

#include "gatt.h"
#include "hci.h"

#include "gapgattserver.h"
#include "gattservapp.h"

#if defined ( PLUS_BROADCASTER )
#include "peripheralBroadcaster.h"
#else
#include "peripheral.h"
#endif

#include "gapbondmgr.h"
#include "gliese.h"
#if defined FEATURE_OAD
#include "oad.h"
#include "oad_target.h"
#endif

// Services
#include "st_util.h"
#include "devinfoservice-st.h"
#include "irtempservice.h"
//#include "accelerometerservice.h"
#include "humidityservice.h"
//#include "magnetometerservice.h"
//#include "barometerservice.h"
//#include "gyroservice.h"
//#include "testservice.h"
//#include "simplekeys.h"
#include "ccservice.h"
#include "battservice.h"
// Sensor drivers
#include "Deimos.h"
#include "hal_sensor.h"

#include "hal_irtemp.h"
//#include "hal_acc.h"
#include "hal_humi.h"
//#include "hal_mag.h"
//#include "hal_bar.h"
//#include "hal_gyro.h"
#include "i2c.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

// How often to perform sensor reads (milliseconds)
#define TEMP_DEFAULT_PERIOD                   1000
#define HUM_DEFAULT_PERIOD                    1000
#define BAR_DEFAULT_PERIOD                    1000
#define TEMP_DEFAULT_PERIOD_ADV               4000
#define HUM_DEFAULT_PERIOD_ADV                5000
#define BAR_DEFAULT_PERIOD_ADV                7000
#define MAG_DEFAULT_PERIOD                    2000
#define ACC_DEFAULT_PERIOD                    1000
#define GYRO_DEFAULT_PERIOD                   1000

// Constants for two-stage reading
#define TEMP_MEAS_DELAY                       50   // Conversion time 26 ms
#define BAR_FSM_PERIOD                        80
#define ACC_FSM_PERIOD                        20
#define HUM_FSM_PERIOD                        20
#define GYRO_STARTUP_TIME                     60    // Start-up time max. 50 ms

// What is the advertising interval when device is discoverable (units of 625us, 160=100ms)
// 1280 * 0.625 = 800ms
// 1600 * 0.625 = 1000ms
// 3200 * 0.625 = 2000ms
// 4800 * 0.625 = 3000ms
// 8000 * 0.625 = 5000ms
// 9600 * 0.625 = 6000ms
// 16000 * 0.625 = 10000ms
// 12191 * 0.625 = 7619.375ms
//#define DEFAULT_ADVERTISING_INTERVAL          3200
#ifdef DEIMOS_BEACON
static uint16 gAdvIntervalMin = 1600;
static uint16 gAdvIntervalMax = 1600;
#else
static uint16 gAdvIntervalMin = 3200;
static uint16 gAdvIntervalMax = 3200;
#endif

// General discoverable mode advertises indefinitely
#define DEFAULT_DISCOVERABLE_MODE             GAP_ADTYPE_FLAGS_GENERAL

/* Apple Connnection Params Limits
 1. Interval Max * (Slave Latency + 1) �� 2 seconds��
 2. Interval Min �� 20 ms��
 3. Interval Min + 20 ms �� Interval Max��
 4. Slave Latency �� 4��
 5. connSupervisionTimeout �� 6 seconds
 6. Interval Max * (Slave Latency + 1) * 3 < connSupervisionTimeout
 */
// Minimum connection interval (units of 1.25ms, 80=100ms) if automatic parameter update request is enabled
// 1500 = 1875ms
// 96 = 120ms
// 176 = 220ms
// 189 = 236.25ms
// 302 = 377.5ms
// 303 = 378.75ms
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     303

// Maximum connection interval (units of 1.25ms, 800=1000ms) if automatic parameter update request is enabled
// 1522 = 1902.5ms
// 190 = 237.5ms
// 192 = 240ms --> On iOS7.0.4, this is the max value!!!
// 200 = 250ms
// 210 = 262.5ms
// 318 = 397.5ms
// 319 = 398.75ms
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     319

// Slave latency to use if automatic parameter update request is enabled
#define DEFAULT_DESIRED_SLAVE_LATENCY         4

// Supervision timeout value (units of 10ms, 1000=10s) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_CONN_TIMEOUT          600

// Whether to enable automatic parameter update request when a connection is formed
#define DEFAULT_ENABLE_UPDATE_REQUEST         FALSE

// Connection Pause Peripheral time value (in seconds)
#define DEFAULT_CONN_PAUSE_PERIPHERAL         4

// Company Identifier: Texas Instruments Inc. (13)
#define TI_COMPANY_ID                         0x000D

#define INVALID_CONNHANDLE                    0xFFFF

// Length of bd addr as a string
#define B_ADDR_STR_LEN                        15

#if defined ( PLUS_BROADCASTER )
#define ADV_IN_CONN_WAIT                    500 // delay 500 ms
#endif

// Side key bit
#define SK_KEY_SIDE                           0x04

// Test mode bit
#define TEST_MODE_ENABLE                      0x80

// Common values for turning a sensor on and off + config/status
#define ST_CFG_SENSOR_DISABLE                 0x00
#define ST_CFG_SENSOR_ENABLE                  0x01
#define ST_CFG_CALIBRATE                      0x02
#define ST_CFG_ERROR                          0xFF

// System reset
#define ST_SYS_RESET_DELAY                    3000

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
static uint8 deimos_TaskID;   // Task ID for internal task/event processing

static gaprole_States_t gapProfileState = GAPROLE_INIT;

// GAP - SCAN RSP data (max size = 31 bytes)
static uint8 scanRspData[] =
{
    // complete name
    0x07,   // length of this data
    GAP_ADTYPE_LOCAL_NAME_COMPLETE,
    0x44,   // 'D'
    0x65,   // 'e'
    0x69,   // 'i'
    0x6D,   // 'm'
    0x6F,   // 'o'
    0x73,   // 's'
#if 0
    // connection interval range
    0x05,   // length of this data
    GAP_ADTYPE_SLAVE_CONN_INTERVAL_RANGE,
    LO_UINT16( DEFAULT_DESIRED_MIN_CONN_INTERVAL ),
    HI_UINT16( DEFAULT_DESIRED_MIN_CONN_INTERVAL ),
    LO_UINT16( DEFAULT_DESIRED_MAX_CONN_INTERVAL ),
    HI_UINT16( DEFAULT_DESIRED_MAX_CONN_INTERVAL ),
#endif
    // Tx power level
    0x02,   // length of this data
    GAP_ADTYPE_POWER_LEVEL,
    0       // 0dBm
};

/* For iBeacon adver data:
# Actual Advertising Data Starts Here
02 01 1a
1a ff 4c 00 02 15 # Apple's static prefix to the advertising data -- this is always the same
e2 c5 6d b5 df fb 48 d2 b0 60 d0 f5 a7 10 96 e0 # iBeacon profileUUID
00 00 # major (LSB first)
00 00 # minor (LSB first)
c5 # The 2's complement of the calibrated Tx Power
For temperature app, I use the minor as temperature.
 */
#if defined(DEIMOS_TEMPERATURE)
// GAP - Advertisement data (max size = 31 bytes, though this is
// best kept short to conserve power while advertisting)
static uint8 advertDataDeimos[] =
{
    0x02,   // length of first data structure (2 bytes excluding length byte)
    GAP_ADTYPE_FLAGS,   // AD Type = Flags
    0x1a,

    0x1b,   // length of second data structure
    0xff, 0x4c, 0x00, 0x02, 0x15, // 8
    // UUID : f501ffb9-f1d1-4acd-b1bc-39df920e29f5
    0xf5, 0x01, 0xff, 0xb9, 0xf1, 0xd1, 0x4a, 0xcd, 0xb1, 0xbc, 0x39, 0xdf, 0x92, 0x0e, 0x29, 0xf5,
    0x03, 0x03, //26, 0x0303(771), is a magic number.
    0x00, 0x00, //28, For temperature app, I use the minor as temperature, two bytes.
    0xc4, // -60dBm == 1m  //29
    0x22, // reserve // 30
};
#else // This is iBeacon
static uint8 advertDataDeimos[] =
{
    0x02,   // length of first data structure (2 bytes excluding length byte)
    GAP_ADTYPE_FLAGS,   // AD Type = Flags
    0x1a,

    0x1b,   // length of second data structure
    0xff, 0x4c, 0x00, 0x02, 0x15, // 8, Apple's static prefix to the advertising data -- this is always the same
    0xFD, 0xA5, 0x06, 0x93, 0xA4, 0xE2, 0x4F, 0xB1, 0xAF, 0xCF, 0xC6, 0xEB, 0x07, 0x64, 0x78, 0x25,
    //0xe2, 0xc5, 0x6d, 0xb5, 0xdf, 0xfb, 0x48, 0xd2, 0xb0, 0x60, 0xd0, 0xf5, 0xa7, 0x10, 0x96, 0xe0,//iBeacon profileUUID
#if 0
    0x00, 0x0a, //26, major (LSB first)
    0x00, 0x07, //28, minor (LSB first)
#else
    0x27, 0x11, //10001
    0xE9, 0xE0, //59872
#endif
    0xc5, // -59dBm == 1m  //29, The 2's complement of the calibrated Tx Power
    0x11, // reserve // 30
};
#endif

#if defined(HAL_IMAGE_A)
#if defined(DEIMOS_TEMPERATURE)
static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "DeimosTEMP_A";
#else
static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "DeimosBeaconA";
#endif
#elif defined(HAL_IMAGE_B)
#if defined(DEIMOS_TEMPERATURE)
static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "DeimosTEMP_B";
#else
static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "DeimosBeaconB";
#endif
#else
#if defined(DEIMOS_TEMPERATURE)
static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "DeimosTEMP";
#else
static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "DeimosBeacon";
#endif
#endif

// Sensor State Variables
static bool   irTempEnabled = FALSE;
//static bool   magEnabled = FALSE;
//static uint8  accConfig = ST_CFG_SENSOR_DISABLE;
//static bool   barEnabled = FALSE;
//static bool   humiEnabled = FALSE;
//static bool   gyroEnabled = FALSE;

//static bool   barBusy = FALSE;
static uint8  humiState = 0;

static bool   sysResetRequest = FALSE;

//static uint16 sensorMagPeriod = MAG_DEFAULT_PERIOD;
//static uint16 sensorAccPeriod = ACC_DEFAULT_PERIOD;
static uint16 sensorTmpPeriod = TEMP_DEFAULT_PERIOD;
//static uint16 sensorHumPeriod = HUM_DEFAULT_PERIOD;
//static uint16 sensorBarPeriod = BAR_DEFAULT_PERIOD;
static uint16 sensorTmpPeriod_Adv = TEMP_DEFAULT_PERIOD_ADV;
static uint16 sensorHumPeriod_Adv = HUM_DEFAULT_PERIOD_ADV;
//static uint16 sensorBarPeriod_Adv = BAR_DEFAULT_PERIOD_ADV;
//static uint16 sensorGyrPeriod = GYRO_DEFAULT_PERIOD;

//static uint8  sensorGyroAxes = 0;
//static bool   sensorGyroUpdateAxes = FALSE;
//static uint16 selfTestResult = 0;
//static bool   testMode = FALSE;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void deimos_ProcessOSALMsg( osal_event_hdr_t *pMsg );
static void peripheralStateNotificationCB( gaprole_States_t newState );

static void readIrTempData( void );
static void readIrTempDataAdv( void );
//static void readHumData( void );
static void readHumDataAdv( void );
//static void readAccData( void );
//static void readMagData( void );
//static void readBarData( void );
//static void readBarDataAdv( void );
//static void readBarCalibration( void );
//static void readGyroData( void );

//static void barometerChangeCB( uint8 paramID );
#if defined(DEIMOS_TEMPERATURE)
static void irTempChangeCB( uint8 paramID );
#endif
//static void accelChangeCB( uint8 paramID );
//static void humidityChangeCB( uint8 paramID);
//static void magnetometerChangeCB( uint8 paramID );
//static void gyroChangeCB( uint8 paramID );
//static void testChangeCB( uint8 paramID );
static void ccChangeCB( uint8 paramID );
static void gapRolesParamUpdateCB( uint16 connInterval, uint16 connSlaveLatency, uint16 connTimeout );

//static void resetSensorSetup( void );
//static void deimos_HandleKeys( uint8 shift, uint8 keys );
static void resetCharacteristicValue( uint16 servID, uint8 paramID, uint8 value, uint8 paramLen );
//static void resetCharacteristicValues( void );
static void glieseServiceChangeCB( uint8 paramID );

/*********************************************************************
 * PROFILE CALLBACKS
 */

// GAP Role Callbacks
static gapRolesCBs_t deimos_PeripheralCBs =
{
    peripheralStateNotificationCB,  // Profile State Change Callbacks
    NULL                            // When a valid RSSI is read from controller (not used by application)
};

// GAP Bond Manager Callbacks
static gapBondCBs_t deimos_BondMgrCBs =
{
    NULL,                     // Passcode callback (not used by application)
    NULL                      // Pairing / Bonding state Callback (not used by application)
};

// Simple GATT Profile Callbacks
/*
static sensorCBs_t deimos_BarometerCBs =
{
  barometerChangeCB,        // Characteristic value change callback
};
*/
#if defined(DEIMOS_TEMPERATURE)
static sensorCBs_t deimos_IrTempCBs =
{
    irTempChangeCB,           // Characteristic value change callback
};
#endif
/*
static sensorCBs_t deimos_AccelCBs =
{
  accelChangeCB,            // Characteristic value change callback
};

static sensorCBs_t deimos_HumidCBs =
{
  humidityChangeCB,         // Characteristic value change callback
};

static sensorCBs_t deimos_MagnetometerCBs =
{
  magnetometerChangeCB,     // Characteristic value change callback
};

static sensorCBs_t deimos_GyroCBs =
{
  gyroChangeCB,             // Characteristic value change callback
};

static testCBs_t deimos_TestCBs =
{
  testChangeCB,             // Charactersitic value change callback
};
*/
static ccCBs_t deimos_ccCBs =
{
    ccChangeCB,               // Charactersitic value change callback
};

static gapRolesParamUpdateCB_t paramUpdateCB =
{
    gapRolesParamUpdateCB,
};

static glieseProfileCBs_t GlieseCBs =
{
    glieseServiceChangeCB,               // Charactersitic value change callback
};
/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      deimos_Init
 *
 * @brief   Initialization function for the Simple BLE Peripheral App Task.
 *          This is called during initialization and should contain
 *          any application specific initialization (ie. hardware
 *          initialization/setup, table initialization, power up
 *          notificaiton ... ).
 *
 * @param   task_id - the ID assigned by OSAL.  This ID should be
 *                    used to send messages and set timers.
 *
 * @return  none
 */
void deimos_Init( uint8 task_id )
{
    deimos_TaskID = task_id;

    // Setup the GAP
    VOID GAP_SetParamValue( TGAP_CONN_PAUSE_PERIPHERAL, DEFAULT_CONN_PAUSE_PERIPHERAL );

    uint8 ret = 1;
#if defined(DEIMOS_BEACON)
    uint8 beacon[GLIESEPROFILE_CHAR2_LEN] = {0};
    ret = osal_snv_read(BLE_NVID_iBeacon_SET, GLIESEPROFILE_CHAR2_LEN, beacon);
    if (ret == SUCCESS)// set iBeacon UUID
    {
        osal_memcpy(advertDataDeimos + 9, beacon, 16);
    }
    GlieseProfile_SetParameter(GLIESEPROFILE_CHAR2, GLIESEPROFILE_CHAR2_LEN, advertDataDeimos + 9);

    uint8 beacon1[GLIESEPROFILE_CHAR1_LEN - 4] = {0};
    ret = osal_snv_read(BLE_NVID_iBeacon_SET1, GLIESEPROFILE_CHAR1_LEN - 4, beacon1);;
    if (ret == SUCCESS)
    {
        // set major
        advertDataDeimos[25] = beacon1[0];
        advertDataDeimos[26] = beacon1[1];

        // set minor
        advertDataDeimos[27] = beacon1[2];
        advertDataDeimos[28] = beacon1[3];
        GlieseProfile_SetParameter(GLIESEPROFILE_CHAR1, GLIESEPROFILE_CHAR1_LEN - 4, beacon1);
        // set The 2's complement of the calibrated Tx Power
        advertDataDeimos[29] = beacon1[4];

        //Tx Power
        uint8 txPower = HCI_EXT_TX_POWER_0_DBM;
        switch (beacon1[5])
        {
        case 0:
            txPower = HCI_EXT_TX_POWER_MINUS_23_DBM;
            break;

        case 1:
            txPower = HCI_EXT_TX_POWER_MINUS_6_DBM;
            break;

        case 2:
            txPower = HCI_EXT_TX_POWER_0_DBM;
            break;

        case 3:
            txPower = HCI_EXT_TX_POWER_0_DBM;
            break;
        }
        HCI_EXT_SetTxPowerCmd( txPower );

        //Advertising Freq (1-100) Unit: 100ms
        if (beacon1[6] != 0)
        {
            gAdvIntervalMin = (uint16)((float)beacon1[6] * (float)100 / (float)0.625);
            if (gAdvIntervalMin < 160)
            {
                gAdvIntervalMin = 160;
            }
            gAdvIntervalMax = gAdvIntervalMin;
        }
    }
#endif

    // Setup the GAP Peripheral Role Profile
    {
        // Device starts advertising upon initialization
        uint8 initial_advertising_enable = TRUE;

        // By setting this to zero, the device will go into the waiting state after
        // being discoverable for 30.72 second, and will not being advertising again
        // until the enabler is set back to TRUE
        uint16 gapRole_AdvertOffTime = 1;
        uint8 enable_update_request = DEFAULT_ENABLE_UPDATE_REQUEST;
        uint16 desired_min_interval = DEFAULT_DESIRED_MIN_CONN_INTERVAL;
        uint16 desired_max_interval = DEFAULT_DESIRED_MAX_CONN_INTERVAL;
        uint16 desired_slave_latency = DEFAULT_DESIRED_SLAVE_LATENCY;
        uint16 desired_conn_timeout = DEFAULT_DESIRED_CONN_TIMEOUT;

        // Set the GAP Role Parameters
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &initial_advertising_enable );
        GAPRole_SetParameter( GAPROLE_ADVERT_OFF_TIME, sizeof( uint16 ), &gapRole_AdvertOffTime );

        GAPRole_SetParameter( GAPROLE_SCAN_RSP_DATA, sizeof ( scanRspData ), scanRspData );
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataDeimos ), advertDataDeimos );

        GAPRole_SetParameter( GAPROLE_PARAM_UPDATE_ENABLE, sizeof( uint8 ), &enable_update_request );
        GAPRole_SetParameter( GAPROLE_MIN_CONN_INTERVAL, sizeof( uint16 ), &desired_min_interval );
        GAPRole_SetParameter( GAPROLE_MAX_CONN_INTERVAL, sizeof( uint16 ), &desired_max_interval );
        GAPRole_SetParameter( GAPROLE_SLAVE_LATENCY, sizeof( uint16 ), &desired_slave_latency );
        GAPRole_SetParameter( GAPROLE_TIMEOUT_MULTIPLIER, sizeof( uint16 ), &desired_conn_timeout );
    }

    // Set the GAP Characteristics
    uint8 deviceNameDeimos[GAP_DEVICE_NAME_LEN] = {0};
    ret = osal_snv_read(BLE_NVID_DEVICE_NAME, GAP_DEVICE_NAME_LEN, deviceNameDeimos);
    if (ret == SUCCESS)
    {
        GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, deviceNameDeimos );
    }
    else
    {
        GGS_SetParameter( GGS_DEVICE_NAME_ATT, sizeof(attDeviceName), attDeviceName );
    }


    // Set advertising interval
    {
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, gAdvIntervalMin );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, gAdvIntervalMax );
    }

    // Setup the GAP Bond Manager
    {
        uint32 passkey = 615; // passkey "000000"
        uint8 pairMode = GAPBOND_PAIRING_MODE_WAIT_FOR_REQ;
        uint8 mitm = TRUE;
        uint8 ioCap = GAPBOND_IO_CAP_DISPLAY_ONLY;
        uint8 bonding = TRUE;

        GAPBondMgr_SetParameter( GAPBOND_DEFAULT_PASSCODE, sizeof ( uint32 ), &passkey );
        GAPBondMgr_SetParameter( GAPBOND_PAIRING_MODE, sizeof ( uint8 ), &pairMode );
        GAPBondMgr_SetParameter( GAPBOND_MITM_PROTECTION, sizeof ( uint8 ), &mitm );
        GAPBondMgr_SetParameter( GAPBOND_IO_CAPABILITIES, sizeof ( uint8 ), &ioCap );
        GAPBondMgr_SetParameter( GAPBOND_BONDING_ENABLED, sizeof ( uint8 ), &bonding );
    }


    // Add services
    GGS_AddService( GATT_ALL_SERVICES );            // GAP
    GATTServApp_AddService( GATT_ALL_SERVICES );    // GATT attributes
    DevInfo_AddService();                           // Device Information Service
#if defined(DEIMOS_TEMPERATURE)
    IRTemp_AddService (GATT_ALL_SERVICES );         // IR Temperature Service
#endif
    //  Accel_AddService (GATT_ALL_SERVICES );          // Accelerometer Service
    //  Humidity_AddService (GATT_ALL_SERVICES );       // Humidity Service
    //  Magnetometer_AddService( GATT_ALL_SERVICES );   // Magnetometer Service
    //  Barometer_AddService( GATT_ALL_SERVICES );      // Barometer Service
    //  Gyro_AddService( GATT_ALL_SERVICES );           // Gyro Service
    //  SK_AddService( GATT_ALL_SERVICES );             // Simple Keys Profile
    //  Test_AddService( GATT_ALL_SERVICES );           // Test Profile
    CcService_AddService( GATT_ALL_SERVICES );      // Connection Control Service
    GlieseProfile_AddService( GATT_ALL_SERVICES );  // Gliese Profile for Phobos
#if defined FEATURE_OAD
    VOID OADTarget_AddService();                    // OAD Profile
#endif

    // Setup the Seensor Profile Characteristic Values
    //resetCharacteristicValues();

    // Register for all key events - This app will handle all key events
    //  RegisterForKeys( deimos_TaskID );

    // makes sure LEDs are off
    //  HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_OFF );

    // Initialise sensor drivers

    //  HalHumiInit();
    //  HalMagInit();
    //  HalAccInit();
    //  HalBarInit();
    //  HalGyroInit();

    // Register callbacks with profile
#if defined(DEIMOS_TEMPERATURE)
    VOID IRTemp_RegisterAppCBs( &deimos_IrTempCBs );
#endif
    //  VOID Magnetometer_RegisterAppCBs( &deimos_MagnetometerCBs );
    //  VOID Accel_RegisterAppCBs( &deimos_AccelCBs );
    //  VOID Humidity_RegisterAppCBs( &deimos_HumidCBs );
    //  VOID Barometer_RegisterAppCBs( &deimos_BarometerCBs );
    //  VOID Gyro_RegisterAppCBs( &deimos_GyroCBs );
    //  VOID Test_RegisterAppCBs( &deimos_TestCBs );
    VOID CcService_RegisterAppCBs( &deimos_ccCBs );
    VOID GAPRole_RegisterAppCBs( &paramUpdateCB );
    // Initialize the ADC for battery reads
    HalAdcInit();
    // Register for Battery service callback;
    //Batt_Register ( phobosRateBattCB );
    GlieseProfile_RegisterAppCBs(&GlieseCBs);

    P0SEL = 0x00; // Configure Port 0 as GPIO
    P1SEL = 0x00; // Configure Port 1 as GPIO
    P2SEL = 0;    // Configure Port 2 as GPIO

    // Port Direction,  0: Input,  1: Output
    P0DIR = 0xFF;
    P1DIR = 0xFF;
    P2DIR = 0x1F;

    P0 = 0x0; // All pins on port 0 to low
    P1 = 0x0; // All pins on port 1 to low
    P2 = 0;   // All pins on port 2 to low
#if defined(DEIMOS_TEMPERATURE)
  //  tempinits();
//    HALIRTempInit();
    Tmp112Init();

#endif
    // Enable clock divide on halt
    // This reduces active current while radio is active and CC254x MCU
    // is halted
    HCI_EXT_ClkDivOnHaltCmd( HCI_EXT_ENABLE_CLK_DIVIDE_ON_HALT );

    // Setup a delayed profile startup
    osal_set_event( deimos_TaskID, ST_START_DEVICE_EVT );
}

/*********************************************************************
 * @fn      deimos_ProcessEvent
 *
 * @brief   Simple BLE Peripheral Application Task event processor.  This function
 *          is called to process all events for the task.  Events
 *          include timers, messages and any other user defined events.
 *
 * @param   task_id  - The OSAL assigned task ID.
 * @param   events - events to process.  This is a bit map and can
 *                   contain more than one event.
 *
 * @return  events not processed
 */
uint16 deimos_ProcessEvent( uint8 task_id, uint16 events )
{
    VOID task_id; // OSAL required parameter that isn't used in this function

    if ( events & SYS_EVENT_MSG )
    {
        uint8 *pMsg;

        if ( (pMsg = osal_msg_receive( deimos_TaskID )) != NULL )
        {
            deimos_ProcessOSALMsg( (osal_event_hdr_t *)pMsg );

            // Release the OSAL message
            VOID osal_msg_deallocate( pMsg );
        }

        // return unprocessed events
        return (events ^ SYS_EVENT_MSG);
    }

    // Handle system reset (long press on side key)
    if ( events & ST_SYS_RESET_EVT )
    {
        if (sysResetRequest)
        {
            if (gapProfileState != GAPROLE_CONNECTED)
            {
                HAL_SYSTEM_RESET();
            }
        }
        return ( events ^ ST_SYS_RESET_EVT );
    }

    if ( events & ST_START_DEVICE_EVT )
    {
        // Start the Device
        VOID GAPRole_StartDevice( &deimos_PeripheralCBs );
        // Start Bond Manager
        VOID GAPBondMgr_Register( &deimos_BondMgrCBs );

        advertDataDeimos[30] = battMeasure();
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataDeimos ), advertDataDeimos );
        osal_start_timerEx( deimos_TaskID, ST_IRTEMPERATURE_READ_ADV_EVT, 2000);
        //osal_start_timerEx( deimos_TaskID, ST_HUMIDITY_SENSOR_ADV_EVT, 10000);
        //    osal_start_timerEx( deimos_TaskID, ST_BAROMETER_SENSOR_ADV_EVT, 3000);
        //    osal_start_timerEx( deimos_TaskID, ST_SET_ADV_INTERVAL_EVT, 7000);
        //osal_start_timerEx( deimos_TaskID, ST_LED_2_BLINK_EVT, 1000);
        return ( events ^ ST_START_DEVICE_EVT );
    }

    if ( events & ST_LED_2_BLINK_EVT)
    {
        HalLedSet(HAL_LED_2, HAL_LED_MODE_BLINK);
        osal_start_timerEx( deimos_TaskID, ST_LED_2_BLINK_EVT, 1000);
        return ( events ^ ST_LED_2_BLINK_EVT );
    }

    if ( events & ST_SET_ADV_INTERVAL_EVT )
    {
        uint8 advState = FALSE;
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
        uint16 advInt = gAdvIntervalMax;
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, advInt );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, advInt );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 0 );
        advState = TRUE;
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
        return ( events ^ ST_SET_ADV_INTERVAL_EVT );
    }
    if ( events & ST_CONN_PARAM_UPDATE_EVT )
    {
        // Send param update.  If it fails, retry until successful.
        GAPRole_SendUpdateParam( DEFAULT_DESIRED_MIN_CONN_INTERVAL, DEFAULT_DESIRED_MAX_CONN_INTERVAL,
                                 DEFAULT_DESIRED_SLAVE_LATENCY, DEFAULT_DESIRED_CONN_TIMEOUT,
                                 GAPROLE_RESEND_PARAM_UPDATE);
        return (events ^ ST_CONN_PARAM_UPDATE_EVT);
    }
    if ( events & ST_BATTERY_CHECK_EVT )
    {
        // perform battery level check
        Batt_MeasLevel( );
        return (events ^ ST_BATTERY_CHECK_EVT);
    }
    //////////////////////////
    //    IR TEMPERATURE    //
    //////////////////////////
    if ( events & ST_IRTEMPERATURE_READ_EVT )
    {
        if ( irTempEnabled )
        {
            if (HalIRTempStatus() == TMP006_DATA_READY)
            {
                readIrTempData();
                osal_start_timerEx( deimos_TaskID, ST_IRTEMPERATURE_READ_EVT, sensorTmpPeriod - TEMP_MEAS_DELAY );
            }
            else if (HalIRTempStatus() == TMP006_OFF)
            {
                HalIRTempTurnOn();
                osal_start_timerEx( deimos_TaskID, ST_IRTEMPERATURE_READ_EVT, TEMP_MEAS_DELAY );
            }
        }
        else
        {
            //Turn off Temperatur sensor
            VOID HalIRTempTurnOff();
            VOID resetCharacteristicValue(IRTEMPERATURE_SERV_UUID, SENSOR_DATA, 0, IRTEMPERATURE_DATA_LEN);
            VOID resetCharacteristicValue(IRTEMPERATURE_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof ( uint8 ));
        }

        return (events ^ ST_IRTEMPERATURE_READ_EVT);
    }
    #if 1
    if ( events & ST_IRTEMPERATURE_READ_ADV_EVT )
    {
        if (Tem112Status() == TMP112_DATA_READY)
        {
            readIrTempDataAdv();
            TMP112OneShotFinished();
            osal_start_timerEx( deimos_TaskID, ST_IRTEMPERATURE_READ_ADV_EVT, sensorTmpPeriod_Adv );
        }
        else if (Tem112Status() == TMP112_OFF)
        {
            TMP112OneShot();
            osal_start_timerEx( deimos_TaskID, ST_IRTEMPERATURE_READ_ADV_EVT, TEMP_MEAS_DELAY );
        }
        return (events ^ ST_IRTEMPERATURE_READ_ADV_EVT);
    }
    #else
    if ( events & ST_IRTEMPERATURE_READ_ADV_EVT )
    {
        if (HalIRTempStatus() == TMP006_DATA_READY)
        {
            readIrTempDataAdv();
            HalTMP112OneShotFinished();
            osal_start_timerEx( deimos_TaskID, ST_IRTEMPERATURE_READ_ADV_EVT, sensorTmpPeriod_Adv );
        }
        else if (HalIRTempStatus() == TMP006_OFF)
        {
            HalTMP112OneShot();
            osal_start_timerEx( deimos_TaskID, ST_IRTEMPERATURE_READ_ADV_EVT, TEMP_MEAS_DELAY );
        }
        return (events ^ ST_IRTEMPERATURE_READ_ADV_EVT);
    }
    #endif

    //////////////////////////
    //    Accelerometer     //
    //////////////////////////
    /*
      if ( events & ST_ACCELEROMETER_SENSOR_EVT )
      {
        if(accConfig != ST_CFG_SENSOR_DISABLE)
        {
          readAccData();
          osal_start_timerEx( deimos_TaskID, ST_ACCELEROMETER_SENSOR_EVT, sensorAccPeriod );
        }
        else
        {
          VOID resetCharacteristicValue( ACCELEROMETER_SERV_UUID, SENSOR_DATA, 0, ACCELEROMETER_DATA_LEN );
          VOID resetCharacteristicValue( ACCELEROMETER_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof ( uint8 ));
        }

        return (events ^ ST_ACCELEROMETER_SENSOR_EVT);
      }
    */
    //////////////////////////
    //      Humidity        //
    //////////////////////////
    /*
      if ( events & ST_HUMIDITY_SENSOR_EVT )
      {
        if (humiEnabled)
        {
          HalHumiExecMeasurementStep(humiState);
          if (humiState == 2)
          {
            readHumData();
            humiState = 0;
            osal_start_timerEx( deimos_TaskID, ST_HUMIDITY_SENSOR_EVT, sensorHumPeriod );
          }
          else
          {
            humiState++;
            osal_start_timerEx( deimos_TaskID, ST_HUMIDITY_SENSOR_EVT, HUM_FSM_PERIOD );
          }
        }
        else
        {
          resetCharacteristicValue( HUMIDITY_SERV_UUID, SENSOR_DATA, 0, HUMIDITY_DATA_LEN);
          resetCharacteristicValue( HUMIDITY_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof ( uint8 ));
        }

        return (events ^ ST_HUMIDITY_SENSOR_EVT);
      }
    */
    if ( events & ST_HUMIDITY_SENSOR_ADV_EVT )
    {
        HalHumiExecMeasurementStep(humiState);
        if (humiState == 2)
        {
            readHumDataAdv();
            humiState = 0;
            osal_start_timerEx( deimos_TaskID, ST_HUMIDITY_SENSOR_ADV_EVT, sensorHumPeriod_Adv);
            HalHumiInit();
        }
        else
        {
            humiState++;
            osal_start_timerEx( deimos_TaskID, ST_HUMIDITY_SENSOR_ADV_EVT, HUM_FSM_PERIOD );
        }
        return (events ^ ST_HUMIDITY_SENSOR_ADV_EVT);
    }
    //////////////////////////
    //      Magnetometer    //
    //////////////////////////
    /*
      if ( events & ST_MAGNETOMETER_SENSOR_EVT )
      {
        if(magEnabled)
        {
          if (HalMagStatus() == MAG3110_DATA_READY)
          {
            readMagData();
          }
          else if (HalMagStatus() == MAG3110_OFF)
          {
            HalMagTurnOn();
          }

          osal_start_timerEx( deimos_TaskID, ST_MAGNETOMETER_SENSOR_EVT, sensorMagPeriod );
        }
        else
        {
          HalMagTurnOff();
          resetCharacteristicValue( MAGNETOMETER_SERV_UUID, SENSOR_DATA, 0, MAGNETOMETER_DATA_LEN);
          resetCharacteristicValue( MAGNETOMETER_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof ( uint8 ));
        }

        return (events ^ ST_MAGNETOMETER_SENSOR_EVT);
      }
    */
    //////////////////////////
    //        Barometer     //
    //////////////////////////
    /*
      if ( events & ST_BAROMETER_SENSOR_EVT )
      {
        if (barEnabled)
        {
          if (barBusy)
          {
            barBusy = FALSE;
            readBarData();
            osal_start_timerEx( deimos_TaskID, ST_BAROMETER_SENSOR_EVT, sensorBarPeriod );
          }
          else
          {
            barBusy = TRUE;
            HalBarStartMeasurement();
            osal_start_timerEx( deimos_TaskID, ST_BAROMETER_SENSOR_EVT, BAR_FSM_PERIOD );
          }
        }
        else
        {
          resetCharacteristicValue( BAROMETER_SERV_UUID, SENSOR_DATA, 0, BAROMETER_DATA_LEN);
          resetCharacteristicValue( BAROMETER_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof ( uint8 ));
          resetCharacteristicValue( BAROMETER_SERV_UUID, SENSOR_CALB, 0, BAROMETER_CALI_LEN);
        }

        return (events ^ ST_BAROMETER_SENSOR_EVT);
      }
      if ( events & ST_BAROMETER_SENSOR_ADV_EVT )
      {
          if (barBusy)
          {
            barBusy = FALSE;
            readBarDataAdv();
            osal_start_timerEx( deimos_TaskID, ST_BAROMETER_SENSOR_ADV_EVT, sensorBarPeriod_Adv);
            HalBarInit();
          }
          else
          {
            barBusy = TRUE;
            HalBarStartMeasurement();
            osal_start_timerEx( deimos_TaskID, ST_BAROMETER_SENSOR_ADV_EVT, BAR_FSM_PERIOD );
          }
          return (events ^ ST_BAROMETER_SENSOR_ADV_EVT);
      }
    */
    //////////////////////////
    //      Gyroscope       //
    //////////////////////////
    /*
      if ( events & ST_GYROSCOPE_SENSOR_EVT )
      {
        uint8 status;

        status = HalGyroStatus();

        if(gyroEnabled)
        {
          if (status == HAL_GYRO_STOPPED)
          {
            HalGyroSelectAxes(sensorGyroAxes);
            HalGyroTurnOn();
            osal_start_timerEx( deimos_TaskID, ST_GYROSCOPE_SENSOR_EVT, GYRO_STARTUP_TIME);
          }
          else
          {
            if(sensorGyroUpdateAxes)
            {
              HalGyroSelectAxes(sensorGyroAxes);
              sensorGyroUpdateAxes = FALSE;
            }

            if (status == HAL_GYRO_DATA_READY)
            {
              readGyroData();
              osal_start_timerEx( deimos_TaskID, ST_GYROSCOPE_SENSOR_EVT, sensorGyrPeriod - GYRO_STARTUP_TIME);
            }
            else
            {
              // Gyro needs to be activated;
              HalGyroWakeUp();
              osal_start_timerEx( deimos_TaskID, ST_GYROSCOPE_SENSOR_EVT, GYRO_STARTUP_TIME);
            }
          }
        }
        else
        {
          HalGyroTurnOff();
          resetCharacteristicValue( GYROSCOPE_SERV_UUID, SENSOR_DATA, 0, GYROSCOPE_DATA_LEN);
          resetCharacteristicValue( GYROSCOPE_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof( uint8 ));
        }

        return (events ^ ST_GYROSCOPE_SENSOR_EVT);
      }
    */
#if defined ( PLUS_BROADCASTER )
    if ( events & ST_ADV_IN_CONNECTION_EVT )
    {
        uint8 turnOnAdv = TRUE;
        // Turn on advertising while in a connection
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &turnOnAdv );

        return (events ^ ST_ADV_IN_CONNECTION_EVT);
    }
#endif // PLUS_BROADCASTER

    // Discard unknown events
    return 0;
}

/*********************************************************************
 * @fn      deimos_test
 *
 * @brief   Run a self-test of the sensor TAG
 *
 * @param   none
 *
 * @return  bitmask of error flags
 */
/*
uint16 deimos_test(void)
{
  selfTestResult = HalSensorTest();
  HalLedSet(HAL_LED_2,HAL_LED_MODE_OFF);

  // Write the self-test result to the test service
  Test_SetParameter( TEST_DATA_ATTR, TEST_DATA_LEN, &selfTestResult);

  return selfTestResult;
}
*/
/*********************************************************************
* Private functions
*/


/*********************************************************************
 * @fn      deimos_ProcessOSALMsg
 *
 * @brief   Process an incoming task message.
 *
 * @param   pMsg - message to process
 *
 * @return  none
 */
static void deimos_ProcessOSALMsg( osal_event_hdr_t *pMsg )
{
    switch ( pMsg->event )
    {
    case KEY_CHANGE:
        //      deimos_HandleKeys( ((keyChange_t *)pMsg)->state, ((keyChange_t *)pMsg)->keys );
        break;

    default:
        // do nothing
        break;
    }
}

/*********************************************************************
 * @fn      deimos_HandleKeys
 *
 * @brief   Handles all key events for this device.
 *
 * @param   shift - true if in shift/alt.
 * @param   keys - bit field for key events. Valid entries:
 *                 HAL_KEY_SW_2
 *                 HAL_KEY_SW_1
 *
 * @return  none
 */
/*
static void deimos_HandleKeys( uint8 shift, uint8 keys )
{
  uint8 SK_Keys = 0;
  VOID shift;  // Intentionally unreferenced parameter

  if (keys & HAL_KEY_SW_1)
  {
    // Reset the system if side key is pressed for more than 3 seconds
    sysResetRequest = TRUE;
    osal_start_timerEx( deimos_TaskID, ST_SYS_RESET_EVT, ST_SYS_RESET_DELAY );

    if (!testMode ) // Side key
    {
      // If device is not in a connection, pressing the side key should toggle
      //  advertising on and off
      if ( gapProfileState != GAPROLE_CONNECTED )
      {
        uint8 current_adv_enabled_status;
        uint8 new_adv_enabled_status;

        // Find the current GAP advertising status
        GAPRole_GetParameter( GAPROLE_ADVERT_ENABLED, &current_adv_enabled_status );

        if( current_adv_enabled_status == FALSE )
        {
          new_adv_enabled_status = TRUE;
        }
        else
        {
          new_adv_enabled_status = FALSE;
        }

        // Change the GAP advertisement status to opposite of current status
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &new_adv_enabled_status );
      }

      if ( gapProfileState == GAPROLE_CONNECTED )
      {
        uint8 adv_enabled = TRUE;

        // Disconnect
        GAPRole_TerminateConnection();
        // Start advertising
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &adv_enabled );
      }
    }
    else
    {
      // Test mode
      if ( keys & HAL_KEY_SW_1 ) // Side key
      {
        SK_Keys |= SK_KEY_SIDE;
      }
    }
  }

  if ( keys & HAL_KEY_SW_2 )   // Carbon S2
  {
    SK_Keys |= SK_KEY_LEFT;
  }

  if ( keys & HAL_KEY_SW_3 )   // Carbon S3
  {
    SK_Keys |= SK_KEY_RIGHT;
  }

  if (!(keys & HAL_KEY_SW_1))
  {
    // Cancel system reset request
    sysResetRequest = FALSE;
  }

  // Set the value of the keys state to the Simple Keys Profile;
  // This will send out a notification of the keys state if enabled
  SK_SetParameter( SK_KEY_ATTR, sizeof ( uint8 ), &SK_Keys );
}
*/

/*********************************************************************
 * @fn      resetSensorSetup
 *
 * @brief   Turn off all sensors that are on
 *
 * @param   none
 *
 * @return  none
 */
/*
static void resetSensorSetup (void)
{
  if (HalIRTempStatus()!=TMP006_OFF || irTempEnabled)
  {
    HalIRTempTurnOff();
    irTempEnabled = FALSE;
  }

  if (accConfig != ST_CFG_SENSOR_DISABLE)
  {
    accConfig = ST_CFG_SENSOR_DISABLE;
  }

  if (HalMagStatus()!=MAG3110_OFF || magEnabled)
  {
    HalMagTurnOff();
    magEnabled = FALSE;
  }

  if (gyroEnabled)
  {
    HalGyroTurnOff();
    gyroEnabled = FALSE;
  }

  if (barEnabled)
  {
    HalBarInit();
    barEnabled = FALSE;
  }

  if (humiEnabled)
  {
    HalHumiInit();
    humiEnabled = FALSE;
  }

  // Reset internal states
//  sensorGyroAxes = 0;
//  sensorGyroUpdateAxes = FALSE;
//  testMode = FALSE;

  // Reset all characteristics values
  resetCharacteristicValues();
}
*/

/*********************************************************************
 * @fn      peripheralStateNotificationCB
 *
 * @brief   Notification from the profile of a state change.
 *
 * @param   newState - new state
 *
 * @return  none
 */
static void peripheralStateNotificationCB( gaprole_States_t newState )
{
    if (gapProfileState == newState)
    {
        return;
    }
#if 1
    if (gapProfileState == GAPROLE_CONNECTED &&
            newState != GAPROLE_CONNECTED)
    {
        uint8 advState = TRUE;
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, gAdvIntervalMin );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, gAdvIntervalMin );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 0 );
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
    }
    // if advertising stopped
    else if ( gapProfileState == GAPROLE_ADVERTISING &&
              newState == GAPROLE_WAITING )
    {
        uint8 advState = TRUE;
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, gAdvIntervalMax );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, gAdvIntervalMax );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 0 );
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
    }
#endif

    switch ( newState )
    {
    case GAPROLE_STARTED:
    {
        // Set the system ID from the bd addr
        uint8 systemId[DEVINFO_SYSTEM_ID_LEN];
        uint8 systemIdBak[DEVINFO_SYSTEM_ID_LEN];
        GAPRole_GetParameter(GAPROLE_BD_ADDR, systemId);
        //        osal_memcpy(&advertDataDeimos[25], systemId, B_ADDR_LEN);
        osal_memcpy(systemIdBak, systemId, DEVINFO_SYSTEM_ID_LEN);

        // shift three bytes up
        systemId[7] = systemId[5];
        systemId[6] = systemId[4];
        systemId[5] = systemId[3];

        // set middle bytes to zero
        systemId[4] = 0;
        systemId[3] = 0;
        DevInfo_SetParameter(DEVINFO_SYSTEM_ID, DEVINFO_SYSTEM_ID_LEN, systemId);

        // Set the serial number from the bd addr.
        uint8 serialNumber[DEVINFO_SERIAL_NUMBER_LEN + 2] = "\0";
        uint8 aNumber;
        uint8 j = 0;
        for (int8 i = B_ADDR_LEN - 1; i >= 0; i--)
        {
            aNumber = systemIdBak[i];
            if (aNumber < 10)
            {
                strcat((char *)serialNumber + j * 2, (const char *)"0");
                _itoa(aNumber, serialNumber + j * 2 + 1, 16);
            }
            else
            {
                _itoa(aNumber, serialNumber + j * 2, 16);
            }

            /*if (osal_memcmp(&aNumber, &Zero, 1) == TRUE)
            {
                strcat((char*)serialNumber+j*2+1, (const char*)"0");
            }*/
            j++;
        }
        DevInfo_SetParameter(DEVINFO_SERIAL_NUMBER, DEVINFO_SERIAL_NUMBER_LEN, serialNumber);
    }
    break;

    case GAPROLE_ADVERTISING:
        break;

    case GAPROLE_CONNECTED:
        // Set timer to update connection parameters
        // 5 seconds should allow enough time for Service Discovery by the collector to finish
        osal_start_timerEx( deimos_TaskID, ST_CONN_PARAM_UPDATE_EVT, 5000);
        break;

    case GAPROLE_WAITING:
        // Link terminated intentionally: reset all sensors
        //      resetSensorSetup();
        //      osal_start_timerEx( deimos_TaskID, ST_IRTEMPERATURE_READ_ADV_EVT, 2000);
        break;

    default:
        break;
    }

    gapProfileState = newState;
}

/*********************************************************************
 * @fn      readAccData
 *
 * @brief   Read accelerometer data
 *
 * @param   none
 *
 * @return  none
 */
/*
static void readAccData(void)
{
  uint8 aData[ACCELEROMETER_DATA_LEN];

  if (HalAccRead(aData))
  {
    Accel_SetParameter( SENSOR_DATA, ACCELEROMETER_DATA_LEN, aData);
  }
}
*/
/*********************************************************************
 * @fn      readMagData
 *
 * @brief   Read magnetometer data
 *
 * @param   none
 *
 * @return  none
 */
/*
static void readMagData( void )
{
  uint8 mData[MAGNETOMETER_DATA_LEN];

  if (HalMagRead(mData))
  {
    Magnetometer_SetParameter(SENSOR_DATA, MAGNETOMETER_DATA_LEN, mData);
  }
}
*/
/*********************************************************************
 * @fn      readHumData
 *
 * @brief   Read humidity data
 *
 * @param   none
 *
 * @return  none
 */
/*
static void readHumData(void)
{
  uint8 hData[HUMIDITY_DATA_LEN];

  if (HalHumiReadMeasurement(hData))
  {
    Humidity_SetParameter( SENSOR_DATA, HUMIDITY_DATA_LEN, hData);
  }
}
*/
static void readHumDataAdv(void)
{
    uint8 hData[HUMIDITY_DATA_LEN];

    if (HalHumiReadMeasurement(hData))
    {
        //Humidity_SetParameter( SENSOR_DATA, HUMIDITY_DATA_LEN, hData);
        advertDataDeimos[11] = hData[0];
        advertDataDeimos[12] = hData[1];
        advertDataDeimos[13] = hData[3];
        advertDataDeimos[14] = hData[2];
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataDeimos ), advertDataDeimos );
    }
}

/*********************************************************************
 * @fn      readBarData
 *
 * @brief   Read barometer data
 *
 * @param   none
 *
 * @return  none
 */
/*
static void readBarData( void )
{
  uint8 bData[BAROMETER_DATA_LEN];

  if (HalBarReadMeasurement(bData))
  {
    Barometer_SetParameter( SENSOR_DATA, BAROMETER_DATA_LEN, bData);
  }
}
static void readBarDataAdv( void )
{
  uint8 bData[BAROMETER_DATA_LEN];

  if (HalBarReadMeasurement(bData))
  {
    //Barometer_SetParameter( SENSOR_DATA, BAROMETER_DATA_LEN, bData);
    advertDataDeimos[15] = bData[0];
    advertDataDeimos[16] = bData[1];
    advertDataDeimos[17] = bData[2];
    advertDataDeimos[18] = bData[3];
    GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataDeimos ), advertDataDeimos );
  }
}
*/

/*********************************************************************
 * @fn      readBarCalibration
 *
 * @brief   Read barometer calibration
 *
 * @param   none
 *
 * @return  none
 */
/*
static void readBarCalibration( void )
{
  uint8* cData = osal_mem_alloc(BAROMETER_CALI_LEN);

  if (cData != NULL )
  {
    HalBarReadCalibration(cData);
    Barometer_SetParameter( SENSOR_CALB, BAROMETER_CALI_LEN, cData);
    osal_mem_free(cData);
  }
}
*/

/*********************************************************************
 * @fn      readIrTempData
 *
 * @brief   Read IR temperature data
 *
 * @param   none
 *
 * @return  none
 */
static void readIrTempData( void )
{
    uint8 tData[IRTEMPERATURE_DATA_LEN];

    if (HalIRTempRead(tData))
    {
        IRTemp_SetParameter( SENSOR_DATA, IRTEMPERATURE_DATA_LEN, tData);
    }
}
static void readIrTempDataAdv( void )
{
    static bool heihei = TRUE;
    //uint8 tData[IRTEMPERATURE_DATA_LEN];
    uint8 tData[TMP112_DATA_LEN];

    //if (HalIRTempRead(tData))
    if (Tmp112Read(tData))
    {
        //IRTemp_SetParameter( SENSOR_DATA, IRTEMPERATURE_DATA_LEN, tData);

        advertDataDeimos[27] = tData[0];
        advertDataDeimos[28] = tData[1];
        //    advertDataDeimos[9] = tData[2];
        //    advertDataDeimos[10] = tData[3];
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataDeimos ), advertDataDeimos );
        if (heihei)
        {
            GlieseProfile_SetParameter(GLIESEPROFILE_CHAR4, 1, &tData[1]);
        }
        else
        {
            GlieseProfile_SetParameter(GLIESEPROFILE_CHAR4, 1, &tData[0]);
        }
        heihei = !heihei;
    }
}

/*********************************************************************
 * @fn      readGyroData
 *
 * @brief   Read gyroscope data
 *
 * @param   none
 *
 * @return  none
 */
/*
static void readGyroData( void )
{
  uint8 gData[GYROSCOPE_DATA_LEN];

  if (HalGyroRead(gData))
  {
    Gyro_SetParameter( SENSOR_DATA, GYROSCOPE_DATA_LEN, gData);
  }
}
*/
/*********************************************************************
 * @fn      barometerChangeCB
 *
 * @brief   Callback from Barometer Service indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
/*
static void barometerChangeCB( uint8 paramID )
{
  uint8 newValue;

  switch( paramID )
  {
    case SENSOR_CONF:
      Barometer_GetParameter( SENSOR_CONF, &newValue );

      switch ( newValue)
      {
      case ST_CFG_SENSOR_DISABLE:
        if (barEnabled)
        {
          barEnabled = FALSE;
          osal_set_event( deimos_TaskID, ST_BAROMETER_SENSOR_EVT);
        }
        break;

      case ST_CFG_SENSOR_ENABLE:
        if(!barEnabled)
        {
          barEnabled = TRUE;
          osal_set_event( deimos_TaskID, ST_BAROMETER_SENSOR_EVT);
        }
        break;

      case ST_CFG_CALIBRATE:
        readBarCalibration();
        break;

      default:
        break;
      }
      break;

  case SENSOR_PERI:
      Barometer_GetParameter( SENSOR_PERI, &newValue );
      sensorBarPeriod = newValue*SENSOR_PERIOD_RESOLUTION;
      break;

    default:
      // should not get here!
      break;
  }
}
*/

/*********************************************************************
 * @fn      irTempChangeCB
 *
 * @brief   Callback from IR Temperature Service indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
#if defined(DEIMOS_TEMPERATURE)
static void irTempChangeCB( uint8 paramID )
{
    uint8 newValue;

    switch (paramID)
    {
    case SENSOR_CONF:
        IRTemp_GetParameter( SENSOR_CONF, &newValue );

        if ( newValue == ST_CFG_SENSOR_DISABLE)
        {
            // Put sensor to sleep
            if (irTempEnabled)
            {
                irTempEnabled = FALSE;
                osal_set_event( deimos_TaskID, ST_IRTEMPERATURE_READ_EVT);
            }
        }
        else if (newValue == ST_CFG_SENSOR_ENABLE)
        {
            if (!irTempEnabled)
            {
                irTempEnabled = TRUE;
                osal_set_event( deimos_TaskID, ST_IRTEMPERATURE_READ_EVT);
            }
        }
        break;

    case SENSOR_PERI:
        IRTemp_GetParameter( SENSOR_PERI, &newValue );
        sensorTmpPeriod = newValue * SENSOR_PERIOD_RESOLUTION;
        break;

    default:
        // Should not get here
        break;
    }
}
#endif

/*********************************************************************
 * @fn      accelChangeCB
 *
 * @brief   Callback from Acceleromter Service indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
/*
static void accelChangeCB( uint8 paramID )
{
  uint8 newValue;

  switch (paramID)
  {
    case SENSOR_CONF:
      Accel_GetParameter( SENSOR_CONF, &newValue );
      if ( newValue == ST_CFG_SENSOR_DISABLE)
      {
        // Put sensor to sleep
        if (accConfig != ST_CFG_SENSOR_DISABLE)
        {
          accConfig = ST_CFG_SENSOR_DISABLE;
          osal_set_event( deimos_TaskID, ST_ACCELEROMETER_SENSOR_EVT);
        }
      }
      else
      {
        if (accConfig == ST_CFG_SENSOR_DISABLE)
        {
          // Start scheduling only on change disabled -> enabled
          osal_set_event( deimos_TaskID, ST_ACCELEROMETER_SENSOR_EVT);
        }
        // Scheduled already, so just change range
        accConfig = newValue;
        HalAccSetRange(accConfig);
      }
      break;

    case SENSOR_PERI:
      Accel_GetParameter( SENSOR_PERI, &newValue );
      sensorAccPeriod = newValue*SENSOR_PERIOD_RESOLUTION;
      break;

    default:
      // Should not get here
      break;
  }
}
*/

/*********************************************************************
 * @fn      magnetometerChangeCB
 *
 * @brief   Callback from Magnetometer Service indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
/*
static void magnetometerChangeCB( uint8 paramID )
{
  uint8 newValue;

  switch (paramID)
  {
    case SENSOR_CONF:
      Magnetometer_GetParameter( SENSOR_CONF, &newValue );

      if ( newValue == ST_CFG_SENSOR_DISABLE )
      {
        if(magEnabled)
        {
          magEnabled = FALSE;
          osal_set_event( deimos_TaskID, ST_MAGNETOMETER_SENSOR_EVT);
        }
      }
      else if ( newValue == ST_CFG_SENSOR_ENABLE )
      {
        if(!magEnabled)
        {
          magEnabled = TRUE;
          osal_set_event( deimos_TaskID, ST_MAGNETOMETER_SENSOR_EVT);
        }
      }
      break;

    case SENSOR_PERI:
      Magnetometer_GetParameter( SENSOR_PERI, &newValue );
      sensorMagPeriod = newValue*SENSOR_PERIOD_RESOLUTION;
      break;

    default:
      // Should not get here
      break;
  }
}
*/
/*********************************************************************
 * @fn      humidityChangeCB
 *
 * @brief   Callback from Humidity Service indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
/*
static void humidityChangeCB( uint8 paramID )
{
  uint8 newValue;

  switch ( paramID)
  {
  case  SENSOR_CONF:
    Humidity_GetParameter( SENSOR_CONF, &newValue );

    if ( newValue == ST_CFG_SENSOR_DISABLE)
    {
      if (humiEnabled)
      {
        humiEnabled = FALSE;
        osal_set_event( deimos_TaskID, ST_HUMIDITY_SENSOR_EVT);
      }
    }

    if ( newValue == ST_CFG_SENSOR_ENABLE )
    {
      if (!humiEnabled)
      {
        humiEnabled = TRUE;
        humiState = 0;
        osal_set_event( deimos_TaskID, ST_HUMIDITY_SENSOR_EVT);
      }
    }
    break;

  case SENSOR_PERI:
    Humidity_GetParameter( SENSOR_PERI, &newValue );
    sensorHumPeriod = newValue*SENSOR_PERIOD_RESOLUTION;
    break;

  default:
    // Should not get here
    break;
  }
}
*/

/*********************************************************************
 * @fn      gyroChangeCB
 *
 * @brief   Callback from GyroProfile indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
/*
static void gyroChangeCB( uint8 paramID )
{
  uint8 newValue;

  switch (paramID) {
  case SENSOR_CONF:
    Gyro_GetParameter( SENSOR_CONF, &newValue );

    if (newValue == 0)
    {
      // All three axes off, put sensor to sleep
      if (gyroEnabled)
      {
        gyroEnabled = FALSE;
        osal_set_event( deimos_TaskID, ST_GYROSCOPE_SENSOR_EVT);
      }
    }
    else
    {
      // Bitmap tells which axis to enable (bit 0: X, but 1: Y, but 2: Z)
      gyroEnabled = TRUE;
      sensorGyroAxes = newValue & 0x07;
      sensorGyroUpdateAxes = TRUE;
      osal_set_event( deimos_TaskID,  ST_GYROSCOPE_SENSOR_EVT);
    }
    break;

  case SENSOR_PERI:
    Gyro_GetParameter( SENSOR_PERI, &newValue );
    sensorGyrPeriod = newValue*SENSOR_PERIOD_RESOLUTION;
    break;

  default:
    // Should not get here
    break;
  }
}
*/

/*********************************************************************
 * @fn      testChangeCB
 *
 * @brief   Callback from Test indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
/*
static void testChangeCB( uint8 paramID )
{
  if( paramID == TEST_CONF_ATTR )
  {
    uint8 newValue;

    Test_GetParameter( TEST_CONF_ATTR, &newValue );

    if (newValue & TEST_MODE_ENABLE)
    {
      testMode = TRUE;
    }
    else
    {
      testMode = FALSE;
    }

    if (testMode)
    {
      // Test mode: possible to operate LEDs. Key hits will cause notifications,
      // side key does not influence connection state
      if (newValue & 0x01)
      {
        HalLedSet(HAL_LED_1,HAL_LED_MODE_ON);
      }
      else
      {
        HalLedSet(HAL_LED_1,HAL_LED_MODE_OFF);
      }

      if (newValue & 0x02)
      {
        HalLedSet(HAL_LED_2,HAL_LED_MODE_ON);
      }
      else
      {
        HalLedSet(HAL_LED_2,HAL_LED_MODE_OFF);
      }
    }
    else
    {
      // Normal mode; make sure LEDs are reset and attribute cleared
      HalLedSet(HAL_LED_1,HAL_LED_MODE_OFF);
      HalLedSet(HAL_LED_2,HAL_LED_MODE_OFF);
      newValue = 0x00;
      Test_SetParameter( TEST_CONF_ATTR, 1, &newValue );
    }
  }
}
*/
/*********************************************************************
 * @fn      ccChangeCB
 *
 * @brief   Callback from Connection Control indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
static void ccChangeCB( uint8 paramID )
{

    // CCSERVICE_CHAR1: read & notify only

    // CCSERVICE_CHAR: requested connection parameters
    if( paramID == CCSERVICE_CHAR2 )
    {
        uint8 buf[CCSERVICE_CHAR2_LEN];
        uint16 minConnInterval;
        uint16 maxConnInterval;
        uint16 slaveLatency;
        uint16 timeoutMultiplier;

        CcService_GetParameter( CCSERVICE_CHAR2, buf );

        minConnInterval = BUILD_UINT16(buf[0], buf[1]);
        maxConnInterval = BUILD_UINT16(buf[2], buf[3]);
        slaveLatency = BUILD_UINT16(buf[4], buf[5]);
        timeoutMultiplier = BUILD_UINT16(buf[6], buf[7]);

        // Update connection parameters
        GAPRole_SendUpdateParam( minConnInterval, maxConnInterval,
                                 slaveLatency, timeoutMultiplier, GAPROLE_NO_ACTION);
    }

    // CCSERVICE_CHAR3: Disconnect request
    if( paramID == CCSERVICE_CHAR3 )
    {
        // Any change in the value will terminate the connection
        GAPRole_TerminateConnection();
    }
}

static void glieseServiceChangeCB( uint8 paramID )
{
    if (paramID == GLIESEPROFILE_CHAR2) // UUID
    {
        uint8 beacon[GLIESEPROFILE_CHAR2_LEN] = {0};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR2, beacon);
        osal_memcpy(advertDataDeimos + 9, beacon, 16);
        osal_snv_write(BLE_NVID_iBeacon_SET, GLIESEPROFILE_CHAR2_LEN, beacon);
        return;
    }

    if ( paramID == GLIESEPROFILE_CHAR1 )
    {
        uint8 beacon[GLIESEPROFILE_CHAR1_LEN] = {0};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR1, beacon);
        uint8 password[4] = {0xFF, 0x02, 0x9D, 0x8E};
        if (FALSE == osal_memcmp(password, beacon + 7, 4))
        {
            return;
        }
        // set major
        advertDataDeimos[25] = beacon[0];
        advertDataDeimos[26] = beacon[1];

        // set minor
        advertDataDeimos[27] = beacon[2];
        advertDataDeimos[28] = beacon[3];
        GlieseProfile_SetParameter(GLIESEPROFILE_CHAR1, GLIESEPROFILE_CHAR1_LEN - 4, beacon);
        // set The 2's complement of the calibrated Tx Power
        advertDataDeimos[29] = beacon[4];

        //Tx Power
        uint8 txPower = HCI_EXT_TX_POWER_0_DBM;
        switch (beacon[5])
        {
        case 0:
            txPower = HCI_EXT_TX_POWER_MINUS_23_DBM;
            break;

        case 1:
            txPower = HCI_EXT_TX_POWER_MINUS_6_DBM;
            break;

        case 2:
            txPower = HCI_EXT_TX_POWER_0_DBM;
            break;

        case 3:
            txPower = HCI_EXT_TX_POWER_0_DBM;
            break;
        }
        HCI_EXT_SetTxPowerCmd( txPower );

        //Advertising Freq (1-100) Unit: 100ms
        if (beacon[6] != 0)
        {
            gAdvIntervalMin = (uint16)((float)beacon[6] * (float)100 / (float)0.625);
            if (gAdvIntervalMin < 160)
            {
                gAdvIntervalMin = 160;
            }
            gAdvIntervalMax = gAdvIntervalMin;
        }
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataDeimos ), advertDataDeimos );

        osal_snv_write(BLE_NVID_iBeacon_SET1, GLIESEPROFILE_CHAR1_LEN - 4, beacon);
        return;
    }

    if ( paramID == GLIESEPROFILE_CHAR3 ) // Device name.
    {
        uint8 buf[GAP_DEVICE_NAME_LEN] = {0};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR3, buf);
        //GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, attDeviceName );
        GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN - 3, buf );
        osal_snv_write(BLE_NVID_DEVICE_NAME, GAP_DEVICE_NAME_LEN, buf);
    }
}

/*********************************************************************
 * @fn      gapRolesParamUpdateCB
 *
 * @brief   Called when connection parameters are updates
 *
 * @param   connInterval - new connection interval
 *
 * @param   connSlaveLatency - new slave latency
 *
 * @param   connTimeout - new connection timeout
 *
 * @return  none
*/
static void gapRolesParamUpdateCB( uint16 connInterval, uint16 connSlaveLatency, uint16 connTimeout )
{
    uint8 buf[CCSERVICE_CHAR1_LEN];

    buf[0] = LO_UINT16(connInterval);
    buf[1] = HI_UINT16(connInterval);
    buf[2] = LO_UINT16(connSlaveLatency);
    buf[3] = HI_UINT16(connSlaveLatency);
    buf[4] = LO_UINT16(connTimeout);
    buf[5] = HI_UINT16(connTimeout);
    CcService_SetParameter(CCSERVICE_CHAR1, sizeof(buf), buf);
}


/*********************************************************************
 * @fn      resetCharacteristicValue
 *
 * @brief   Initialize a characteristic value to zero
 *
 * @param   servID - service ID (UUID)
 *
 * @param   paramID - parameter ID of the value is to be cleared
 *
 * @param   vakue - value to initialise with
 *
 * @param   paramLen - length of the parameter
 *
 * @return  none
 */
static void resetCharacteristicValue(uint16 servUuid, uint8 paramID, uint8 value, uint8 paramLen)
{
    uint8 *pData = osal_mem_alloc(paramLen);

    if (pData == NULL)
    {
        return;
    }

    osal_memset(pData, value, paramLen);

    switch(servUuid)
    {
    case IRTEMPERATURE_SERV_UUID:
        IRTemp_SetParameter( paramID, paramLen, pData);
        break;
        /*
            case ACCELEROMETER_SERV_UUID:
              Accel_SetParameter( paramID, paramLen, pData);
              break;

            case MAGNETOMETER_SERV_UUID:
              Magnetometer_SetParameter( paramID, paramLen, pData);
              break;
        */
    case HUMIDITY_SERV_UUID:
        Humidity_SetParameter( paramID, paramLen, pData);
        break;
        /*
            case BAROMETER_SERV_UUID:
              Barometer_SetParameter( paramID, paramLen, pData);
              break;

            case GYROSCOPE_SERV_UUID:
              Gyro_SetParameter( paramID, paramLen, pData);
              break;
        */
    default:
        // Should not get here
        break;
    }

    osal_mem_free(pData);
}

/*********************************************************************
 * @fn      resetCharacteristicValues
 *
 * @brief   Initialize all the characteristic values
 *
 * @return  none
 */
/*
static void resetCharacteristicValues( void )
{
  resetCharacteristicValue( IRTEMPERATURE_SERV_UUID, SENSOR_DATA, 0, IRTEMPERATURE_DATA_LEN);
  resetCharacteristicValue( IRTEMPERATURE_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof ( uint8 ));
  resetCharacteristicValue( IRTEMPERATURE_SERV_UUID, SENSOR_PERI, TEMP_DEFAULT_PERIOD / SENSOR_PERIOD_RESOLUTION, sizeof ( uint8 ));

  resetCharacteristicValue( ACCELEROMETER_SERV_UUID, SENSOR_DATA, 0, ACCELEROMETER_DATA_LEN );
  resetCharacteristicValue( ACCELEROMETER_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof ( uint8 ));
  resetCharacteristicValue( ACCELEROMETER_SERV_UUID, SENSOR_PERI, ACC_DEFAULT_PERIOD / SENSOR_PERIOD_RESOLUTION, sizeof ( uint8 ));

  resetCharacteristicValue( HUMIDITY_SERV_UUID, SENSOR_DATA, 0, HUMIDITY_DATA_LEN);
  resetCharacteristicValue( HUMIDITY_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof ( uint8 ));
  resetCharacteristicValue( HUMIDITY_SERV_UUID, SENSOR_PERI, HUM_DEFAULT_PERIOD / SENSOR_PERIOD_RESOLUTION, sizeof ( uint8 ));

  resetCharacteristicValue( MAGNETOMETER_SERV_UUID, SENSOR_DATA, 0, MAGNETOMETER_DATA_LEN);
  resetCharacteristicValue( MAGNETOMETER_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof ( uint8 ));
  resetCharacteristicValue( MAGNETOMETER_SERV_UUID, SENSOR_PERI, MAG_DEFAULT_PERIOD / SENSOR_PERIOD_RESOLUTION, sizeof ( uint8 ));

  resetCharacteristicValue( BAROMETER_SERV_UUID, SENSOR_DATA, 0, BAROMETER_DATA_LEN);
  resetCharacteristicValue( BAROMETER_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof ( uint8 ));
  resetCharacteristicValue( BAROMETER_SERV_UUID, SENSOR_PERI, BAR_DEFAULT_PERIOD / SENSOR_PERIOD_RESOLUTION, sizeof ( uint8 ));

  resetCharacteristicValue( GYROSCOPE_SERV_UUID, SENSOR_DATA, 0, GYROSCOPE_DATA_LEN);
  resetCharacteristicValue( GYROSCOPE_SERV_UUID, SENSOR_CONF, ST_CFG_SENSOR_DISABLE, sizeof( uint8 ));
  resetCharacteristicValue( GYROSCOPE_SERV_UUID, SENSOR_PERI, GYRO_DEFAULT_PERIOD / SENSOR_PERIOD_RESOLUTION, sizeof ( uint8 ));

}
*/

/*********************************************************************
*********************************************************************/

